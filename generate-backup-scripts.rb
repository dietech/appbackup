#!/usr/bin/env ruby

require "erb"
require "yaml"

#############################
# Generel Setup
#############################

#############################
# Setup for each App as
# defined in apps_to_backup.yml
#############################
yaml_file = File.join(File.dirname(__FILE__), "/apps_to_backup.yml")
raise "File not found: #{yaml_file}" unless File.exist?(yaml_file)

@apps = YAML.load_file(yaml_file)
@backup_dir = @apps.delete("base_backup_directory") || "~/backups"

@period = "daily"
@suffix = "daily-`LANG=en_US date +%A`"
daily_backup_template = ERB.new File.new("scripts/backup.sh.erb").read
daily_backup = daily_backup_template.result(binding)

File.write("scripts/backup-daily.sh", daily_backup)
File.chmod(0o700, "scripts/backup-daily.sh")

@period = "monthly"
@suffix = "monthly-`LANG=en_US date +%B`"
monthly_backup_template = ERB.new File.new("scripts/backup.sh.erb").read
monthly_backup = monthly_backup_template.result(binding)

File.write("scripts/backup-monthly.sh", monthly_backup)
File.chmod(0o700, "scripts/backup-monthly.sh")

@period = "now"
@suffix = "now-`LANG=en_US date --rfc-3339=date`"
now_backup_template = ERB.new File.new("scripts/backup.sh.erb").read
now_backup = now_backup_template.result(binding)

File.write("scripts/backup-now.sh", now_backup)
File.chmod(0o700, "scripts/backup-now.sh")
